package postgres

type Event struct {
  Id int
  Game int
  Name string
  Tournamentname string
  StartAt int
  EndAt int
  VenueName string
  VenueAddress string
  MapsPlaceId string
  Url string
}

type HeadToHeadSet struct {
  Id int
  EventID int
  CompletedAt int
  PlayerOneId int
  PlayerTwoId int
  PlayerOneScore int
  PlayerTwoScore int
  WinnerId int
  BracketRound int
  FullRoundText string
  Tournamentname string
  EventName string
  PlayerOneName string
  PlayerTwoName string
  PlayerOnePrefix string
  PlayerTwoPrefix string
  PlayerOneCountry string
  PlayerTwoCountry string
}

type Player struct {
  Id int
  Prefix string
  Name string
  Twitter string
  Twitch string
  Country string
}

type Set struct {
  Id int
  EventID int
  CompletedAt int
  PlayerOneId int
  PlayerTwoId int
  PlayerOneScore int
  PlayerTwoScore int
  PlayerOneEventPrefix string
  PlayerTwoEventPrefix string
  WinnerId int
  BracketRound int
  FullRoundText string
}
