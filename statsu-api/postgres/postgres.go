package postgres

import (
  "github.com/jmoiron/sqlx"
  "database/sql"
  "fmt"
)

func EvaluateWinner(winnerId int, playerOneEntrantId int, playerTwoEntrantId int, playerOneId int, playerTwoId int) int {
  if(winnerId == playerOneEntrantId) {
    return playerOneId
  } else {
    return playerTwoId
  }
}

func InsertEvent(tx *sqlx.Tx, event *Event) (sql.Result, error) {
  return tx.NamedExec(`
    INSERT INTO events (
      id,
      game,
      name,
      tournamentname,
      startat,
      endat,
      venuename,
      venueaddress,
      mapsplaceid,
      smashurl
    ) VALUES (
      :id,
      :game,
      :name,
      :tournamentname,
      :startat,
      :endat,
      :venuename,
      :venueaddress,
      :mapsplaceid,
      :url
    ) ON CONFLICT DO NOTHING`,
    &event,
  )
}

func InsertPlayer(tx *sqlx.Tx, player *Player) (sql.Result, error) {
  return tx.NamedExec(`
    INSERT INTO players (
      id,
      prefix,
      name,
      twitter,
      twitch,
      country
    ) VALUES (
      :id,
      :prefix,
      :name,
      :twitter,
      :twitch,
      :country
    ) ON CONFLICT DO NOTHING`,
    &player,
  )
}

func InsertSet(tx *sqlx.Tx, set *Set) (sql.Result, error) {
  return tx.NamedExec(`
    INSERT INTO sets (
      id,
      eventid,
      completedat,
      playeroneid,
      playertwoid,
      playeronescore,
      playertwoscore,
      playeroneeventprefix,
      playertwoeventprefix,
      winnerid,
      bracketround,
      fullroundtext
    ) VALUES (
      :id,
      :eventid,
      :completedat,
      :playeroneid,
      :playertwoid,
      :playeronescore,
      :playertwoscore,
      :playeroneeventprefix,
      :playertwoeventprefix,
      :winnerid,
      :bracketround,
      :fullroundtext )
    ON CONFLICT DO NOTHING`,
    &set,
  )
}

func SearchPlayerName(db *sqlx.DB, searchTerm string) ([]Player, error) {
  rows, err := db.NamedQuery(`
    SELECT * FROM players WHERE
      name ILIKE '%'||:name_search||'%'
    ORDER BY
      similarity(name, :name_search) DESC
    LIMIT 10
  `, map[string]interface{}{"name_search": searchTerm})
  player := Player{}
  players := []Player{}
  if err != nil {
    return players, err
  }
  for rows.Next() {
    err = rows.StructScan(&player)
    if err != nil {
      return players, err
    }
    players = append(players, player)
  }
  return players, err
}

func SelectHeadToHeadSets(db *sqlx.DB, searchTermOne int, searchTermTwo int) ([]HeadToHeadSet, error) {
  rows, err := db.NamedQuery(`
    SELECT
      sets.id,
      sets.eventid,
      sets.completedat,
      sets.playeroneid,
      sets.playertwoid,
      sets.playeronescore,
      sets.playertwoscore,
      sets.playeroneeventprefix as playeroneprefix,
      sets.playertwoeventprefix as playertwoprefix,
      sets.winnerid,
      sets.bracketround,
      sets.fullroundtext,
      events.tournamentname,
      events.name as eventname,
      playerone.name as playeronename,
      playertwo.name as playertwoname,
      playerone.country as playeronecountry,
      playertwo.country as playertwocountry
    FROM sets
    INNER JOIN events on sets.eventid = events.id
    LEFT JOIN players as playerone on sets.playeroneid = playerone.id
    LEFT JOIN players as playertwo on sets.playertwoid = playertwo.id
    WHERE
      ( playeroneid=:to AND playertwoid=:tt )
    OR
      ( playeroneid=:tt AND playertwoid=:to )
    ORDER BY completedAt DESC
  `, map[string]interface{}{"to": searchTermOne, "tt": searchTermTwo})
  set := HeadToHeadSet{}
  sets := []HeadToHeadSet{}
  if err != nil {
    fmt.Println(err)
    return sets, err
  }
  for rows.Next() {
    err = rows.StructScan(&set)
    if err != nil {
      fmt.Println(err)
      return sets, err
    }
    sets = append(sets, set)
  }
  return sets, err
}
