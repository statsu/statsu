package main

import (
  "bytes"
  "errors"
  "net/http"
  "net/http/httptest"
  "testing"
  "github.com/stretchr/testify/assert"
  "gitlab.com/statsu/statsu/statsu-api/postgres"
  "github.com/jmoiron/sqlx"
)

func TestPlayerNameSearchRouteSuccess(t *testing.T) {
	router := setupRouter()
  w := httptest.NewRecorder()

  testRequest := []byte(`{"Name": "Gamer"}`)
  searchPlayerName = func(db *sqlx.DB, name string) ([]postgres.Player, error) {
    empty := []postgres.Player{}
    assert.Equal(t, "Gamer", name)
    return empty, nil
  }

	req, _ := http.NewRequest(
    "POST", "/api/players/search",
    bytes.NewBuffer(testRequest),
  )
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
}

func TestPlayerNameSearchRouteDBFail(t *testing.T) {
  router := setupRouter()
  w := httptest.NewRecorder()

  testRequest := []byte(`{"Name": "Gamer"}`)
  searchPlayerName = func(db *sqlx.DB, name string) ([]postgres.Player, error) {
    return nil, errors.New("Test error - DB")
  }

  req, _ := http.NewRequest(
    "POST", "/api/players/search",
    bytes.NewBuffer(testRequest),
  )
	router.ServeHTTP(w, req)

	assert.Equal(t, 501, w.Code)
}

func TestPlayerNameSearchRouteBadRequest(t *testing.T) {
  router := setupRouter()
  w := httptest.NewRecorder()

  malformedTestRequest := []byte(`{Name: 123}`)

  req, _ := http.NewRequest(
    "POST", "/api/players/search",
    bytes.NewBuffer(malformedTestRequest),
  )
  router.ServeHTTP(w, req)

  assert.Equal(t, 400, w.Code)
}
