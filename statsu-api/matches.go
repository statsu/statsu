package main

import (
  "gitlab.com/statsu/statsu/statsu-api/postgres"
  "github.com/gin-gonic/gin"
  "fmt"
)

type HeadToHeadSearchRequest struct {
  SearchIdOne int
  SearchIdTwo int
}

var selectHeadToHeadSets = postgres.SelectHeadToHeadSets

func searchHeadToHeadHandler(c *gin.Context) {
  var headToHeadSearchRequest HeadToHeadSearchRequest
  if c.BindJSON(&headToHeadSearchRequest) == nil {
    matches, err := selectHeadToHeadSets(
      db,
      headToHeadSearchRequest.SearchIdOne,
      headToHeadSearchRequest.SearchIdTwo,
    )
    if err != nil {
      fmt.Println(err)
      c.JSON(501, gin.H{
        "error": "Something bad happened while attempting to access the database.",
      })
    } else {
      c.JSON(200, gin.H{
        "data": matches,
      })
    }
  } else {
    c.JSON(400, gin.H{
      "error": "The request could not be parsed.",
    })
  }
}
