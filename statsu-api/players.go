package main

import (
  "gitlab.com/statsu/statsu/statsu-api/postgres"
  "github.com/gin-gonic/gin"
)

type SearchRequest struct {
  Name string
}

var searchPlayerName = postgres.SearchPlayerName

func searchPlayerNameHandler(c *gin.Context) {
  var searchRequest SearchRequest
  if c.BindJSON(&searchRequest) == nil {
    players, err := searchPlayerName(db, searchRequest.Name)
    if err != nil {
      c.JSON(501, gin.H{
        "error": "Something bad happened while attempting to access the database.",
      })
    } else {
      c.JSON(200, gin.H{
        "data": players,
      })
    }
  } else {
    c.JSON(400, gin.H{
      "error": "The request could not be parsed.",
    })
  }
}
