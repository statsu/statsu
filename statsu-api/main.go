package main

import (
  "github.com/gin-gonic/gin"
  _ "github.com/lib/pq"
  "github.com/jmoiron/sqlx"
  "github.com/gin-contrib/cors"
  "os"
  "fmt"
  "errors"
)

var db *sqlx.DB

func setupRouter() *gin.Engine {
  r := gin.Default()
  if(os.Getenv("STATSU_ENVIRONMENT") != "release") {
    r.Use(cors.New(cors.Config{
      AllowOrigins:     []string{"http://localhost:5000", "http://10.178.104.103:5000"},
      AllowMethods:     []string{"GET", "PUT", "PATCH", "POST"},
      AllowHeaders:     []string{"Origin"},
      ExposeHeaders:    []string{"Content-Length"},
      AllowCredentials: true,
    }))
  } else {
    gin.SetMode(gin.ReleaseMode)
    r.Use(cors.New(cors.Config{
      AllowOrigins:     []string{"http://statsu.gg", "http://www.statsu.gg"},
      AllowMethods:     []string{"GET", "PUT", "PATCH", "POST"},
      AllowHeaders:     []string{"Origin"},
      ExposeHeaders:    []string{"Content-Length"},
      AllowCredentials: true,
    }))
  }
  api := r.Group("/api")
    {
      players := api.Group("/players")
      {
        players.POST("/search", searchPlayerNameHandler)
      }
      matches := api.Group("/matches")
      {
        matches.POST("/h2hsearch", searchHeadToHeadHandler)
      }
    }
    return r
}
func main() {
  dbHost := os.Getenv("DB_HOST")
  dbUser := os.Getenv("DB_USER")
  dbName := os.Getenv("DB_NAME")
  dbPass := os.Getenv("DB_PASS")
  err := errors.New("")
  connection := fmt.Sprintf("host=%s user=%s dbname=%s password=%s sslmode=disable", dbHost, dbUser, dbName, dbPass)
  fmt.Println(connection)
  db, err = sqlx.Open("postgres", connection)
  if err != nil {
    fmt.Println(err)
  }
  r := setupRouter()
  r.Run()
}
