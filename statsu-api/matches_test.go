package main

import (
  "bytes"
  "errors"
  "net/http"
  "net/http/httptest"
  "testing"
  "github.com/stretchr/testify/assert"
  "gitlab.com/statsu/statsu/statsu-api/postgres"
  "github.com/jmoiron/sqlx"
)

func TestH2HSearchRouteSuccess(t *testing.T) {
	router := setupRouter()
  w := httptest.NewRecorder()

  testRequest := []byte(`{"SearchIdOne": 69420, "SearchIdTwo": 42069}`)
  selectHeadToHeadSets = func(db *sqlx.DB, searchTermOne int, searchTermTwo int) ([]postgres.HeadToHeadSet, error) {
    empty := []postgres.HeadToHeadSet{}
    assert.Equal(t, 69420, searchTermOne)
    assert.Equal(t, 42069, searchTermTwo)
    return empty, nil
  }

	req, _ := http.NewRequest(
    "POST", "/api/matches/h2hsearch",
    bytes.NewBuffer(testRequest),
  )
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
}

func TestH2HSearchRouteDBFail(t *testing.T) {
  router := setupRouter()
  w := httptest.NewRecorder()

  testRequest := []byte(`{"SearchIdOne": 69420, "SearchIdTwo": 42069}`)
  selectHeadToHeadSets = func(db *sqlx.DB, searchTermOne int, searchTermTwo int) ([]postgres.HeadToHeadSet, error) {
    return nil, errors.New("Test error - DB")
  }

  req, _ := http.NewRequest(
    "POST", "/api/matches/h2hsearch",
    bytes.NewBuffer(testRequest),
  )
	router.ServeHTTP(w, req)

	assert.Equal(t, 501, w.Code)
}

func TestH2HSearchRouteBadRequest(t *testing.T) {
  router := setupRouter()
  w := httptest.NewRecorder()

  malformedTestRequest := []byte(`{"SearchIOne": "malformed", "SarchIdTwo": REQUEST}`)

  req, _ := http.NewRequest(
    "POST", "/api/matches/h2hsearch",
    bytes.NewBuffer(malformedTestRequest),
  )
  router.ServeHTTP(w, req)

  assert.Equal(t, 400, w.Code)
}
