import '@testing-library/jest-dom/extend-expect'
import { act, render, fireEvent } from '@testing-library/svelte'

import H2HSearch from './H2HSearch.svelte';
import {
  h2hPlayerOne,
  h2hPlayerTwo
} from '../store/values.js';

let h2hPlayerOneValue;
let h2hPlayerTwoValue;

h2hPlayerOne.subscribe(value => {
  h2hPlayerOneValue = value;
});
h2hPlayerTwo.subscribe(value => {
  h2hPlayerTwoValue = value;
});

let results;
let searchButton;

beforeEach(() => {
  fetch.resetMocks();
  results = render(H2HSearch);
  searchButton = () => {
    return results.queryByText('Search Matches')
  }
  h2hPlayerOne.set("");
  h2hPlayerTwo.set("");
});

test('The search button should be hidden unless two search values are not in the store', async () => {
  expect(searchButton()).toBeNull();
  await act(h2hPlayerOne.set("Player One Name"));
  expect(searchButton()).toBeNull();
  await act(h2hPlayerTwo.set("Player Two Name"));
  expect(searchButton()).not.toBeNull();
});

test('clicking the button should fire the h2hsearch method', async () => {
  fetch.mockResponseOnce(JSON.stringify([{}]));
  await act(h2hPlayerOne.set("Player One Name"));
  await act(h2hPlayerTwo.set("Player Two Name"));
  await fireEvent.click(searchButton());
  await results.rerender();
  expect(fetch.mock.calls.length).toEqual(1)
});
