import { writable } from "svelte/store";
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render } from '@testing-library/svelte';
import { screen, waitForElementToBeRemoved } from '@testing-library/dom';
import SearchField from './SearchField.svelte';

let results;

const loadingLiQuery = dom => dom.queryByText('...Loading');
const inputQuery = dom => dom.getByPlaceholderText('Search');
const autocompleteUlQuery = dom => dom.queryByTestId('autocomplete-results');
const suggestionsLiQuery = dom => dom.queryAllByTestId('autocomplete-result');

const displayValue = "Name";
const storeFieldName = "Id";
const store = writable("");
let storeValue = "";
store.subscribe(value => {
  storeValue = value;
});

const searchPlayerResponse = {
  data: [
    {
      Id: 69420,
      Name: "SixtyNine-420",
      Prefix: "KAPPA",
      Country: "Internet",
    },
    {
      Id: 42069,
      Name: "FourTwenty-69",
      Prefix: "FOG",
      Country: "World",
    },
  ]
};

beforeEach(() => {
  fetch.resetMocks();
  results = render(SearchField, {
    displayValue: displayValue,
    storeFieldName: storeFieldName,
    storeField: store,
    debounceTime: 0 // defer til next tick, don't want crazy sleep functions up in here
  });
  store.set("");
});

test('Input should trigger a search attempt / clicking a result should set store', async () => {
  fetch.mockResponseOnce(JSON.stringify(searchPlayerResponse));
  await fireEvent.input(inputQuery(results), { target: { value: 'e' } });
  expect(autocompleteUlQuery(results)).not.toHaveClass('hide-results');
  expect(loadingLiQuery(results)).not.toBeNull();
  await waitForElementToBeRemoved( () => loadingLiQuery(results) );
  expect(fetch.mock.calls.length).toBe(1);
  expect(suggestionsLiQuery(results).length).toBe(searchPlayerResponse.data.length);
  await fireEvent.click(suggestionsLiQuery(results)[0]);
  expect(storeValue).toBe(searchPlayerResponse.data[0].Id);
  expect(inputQuery(results)).toHaveValue(searchPlayerResponse.data[0].Name);
});

test('Empty (by any interpretation) input should not result in a search attempt, and should clear any existing search state', async () => {
  fetch.mockResponseOnce(JSON.stringify(searchPlayerResponse));
  store.set("A value once searched")
  results.rerender({
    props: {
      displayValue: displayValue,
      storeFieldName: storeFieldName,
      storeField: store,
    }
  });
  await fireEvent.input(inputQuery(results), { target: { value: '' } });
  expect(fetch.mock.calls.length).toBe(0);
  expect(autocompleteUlQuery(results)).toHaveClass('hide-results');
  expect(storeValue).toBe("");
});
