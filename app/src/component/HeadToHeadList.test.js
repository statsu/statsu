import '@testing-library/jest-dom/extend-expect'
import { render } from '@testing-library/svelte'

import HeadToHeadList from './HeadToHeadList.svelte';

let results;

const emptyContentQuery = dom => dom.queryByText(/No\ Head\ to\ Head/ig);
const loadingContentQuery = dom => dom.queryByText('...Loading');

beforeEach(() => {
  results = render(HeadToHeadList);
});

test('if loading prop is true, should see loading content', async () => {
  expect(emptyContentQuery(results)).toBeNull();
  expect(loadingContentQuery(results)).toBeNull();
  await results.rerender({ props: { loading: true } });
  expect(emptyContentQuery(results)).toBeNull();
  expect(loadingContentQuery(results)).not.toBeNull();
});

test('if empty prop is true, should see empty content', async () => {
  expect(emptyContentQuery(results)).toBeNull();
  expect(loadingContentQuery(results)).toBeNull();
  await results.rerender({ props: { empty: true } });
  expect(emptyContentQuery(results)).not.toBeNull();
  expect(loadingContentQuery(results)).toBeNull();
});
