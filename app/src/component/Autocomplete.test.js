import '@testing-library/jest-dom/extend-expect'
import { render, fireEvent } from '@testing-library/svelte'

import Autocomplete from './Autocomplete.svelte';

const autocompleteUlQuery = dom => dom.queryByTestId('autocomplete-results');
const inputQuery = dom => dom.getByPlaceholderText('Search');
const loadingLiQuery = dom => dom.queryByText('...Loading');
const suggestionsLiQuery = dom => dom.queryAllByTestId('autocomplete-result');

let results;
const fiveSuggestions = ["zero", "one", "two", "three", "four"];
const fiveNestedSuggestions = [{value: "nested zero"}, {value: "nested one"}, {value: "nested two"}, {value: "nested three"}, {value: "nested four"}];
const getNestedSuggestion = (list, index) => list[index].value;

beforeEach(() => {
  results = render(Autocomplete);
});

test('A truthy loading prop should result in the loading <li> rendering', async () => {
  expect(loadingLiQuery(results)).toBeNull();
  await results.rerender({ props: { loading: true } });
  expect(loadingLiQuery(results)).not.toBeNull();
})

test('Changing the isOpen prop should be reflected in the autocomplete <ul> class set', async () => {
  expect(autocompleteUlQuery(results)).toHaveClass('hide-results');
  await results.rerender({ props: { isOpen: true } });
  expect(autocompleteUlQuery(results)).not.toHaveClass('hide-results');
});

test('Passing in a list of suggestions should render the suggestion list / clicking should call close function with index', async () => {
  expect(suggestionsLiQuery(results).length).toBe(0);
  await results.rerender({ props: { suggestions: fiveSuggestions } });
  expect(suggestionsLiQuery(results).length).toBe(5);
  expect(suggestionsLiQuery(results)[0]).toHaveTextContent("zero");
  await fireEvent.click(suggestionsLiQuery(results)[0]);
  expect(inputQuery(results)).toHaveValue("zero");
});

test('Passing in a custom getSuggestedValue function should override default behavior and allow for more complex suggestion data structures', async () => {
  await results.rerender({ props:{ suggestions: fiveNestedSuggestions, getSuggestedValue: getNestedSuggestion }});
  expect(suggestionsLiQuery(results).length).toBe(5);
  await fireEvent.click(suggestionsLiQuery(results)[0]);
  expect(inputQuery(results)).toHaveValue("nested zero");
});

test('Change events should be evaluated to decide if the suggestions list should be shown', async () => {
  expect(autocompleteUlQuery(results)).toHaveClass('hide-results');
  await fireEvent.input(inputQuery(results), { target: { value: "a" } });
  expect(autocompleteUlQuery(results)).not.toHaveClass('hide-results');
  await fireEvent.input(inputQuery(results), { target: { value: "" } });
  expect(autocompleteUlQuery(results)).toHaveClass('hide-results');
});
