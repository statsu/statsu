import '@testing-library/jest-dom/extend-expect'
import { render } from '@testing-library/svelte'

import HeadToHeadCard from './HeadToHeadCard.svelte';

let results;

const playerOneNameQuery = dom => dom.queryByTestId('player-one-name');
const playerTwoNameQuery = dom => dom.queryByTestId('player-two-name');
const playerOnePrefixQuery = dom => dom.queryByTestId('player-one-prefix');
const playerTwoPrefixQuery = dom => dom.queryByTestId('player-two-prefix');
const playerOneScoreQuery = dom => dom.queryByTestId('player-one-score');
const playerTwoScoreQuery = dom => dom.queryByTestId('player-two-score');
const winnerWedgeQuery = dom => dom.queryByTestId('winner-wedge');

const cardQuery = dom => {
  return {
    player: {
      one: {
        name: playerOneNameQuery(dom),
        prefix: playerOnePrefixQuery(dom),
        score: playerOneScoreQuery(dom),
      },
      two: {
        name: playerTwoNameQuery(dom),
        prefix: playerTwoPrefixQuery(dom),
        score: playerTwoScoreQuery(dom),
      },
    },
    winner: {
      wedge: winnerWedgeQuery(dom),
    },
  }
};

const matchWithZeroScore = {
  Id: 1,
  EventID: 1,
  CompletedAt: 1,
  PlayerOneId: 69420,
  PlayerTwoId: 42069,
  PlayerOneScore: 3,
  PlayerTwoScore: 0,
  WinnerId: 69420,
  BracketRound: -6,
  FullRoundText: "Losers' Semi-Finals",
  Tournamentname: "Evolution",
  EventName: "Street Fighter V",
  PlayerOneName: "SixtyNine-420",
  PlayerTwoName: "FourTwenty-69",
  PlayerOnePrefix: "KAPPA",
  PlayerTwoPrefix: "FOG",
  PlayerOneCountry: "Internet",
  PlayerTwoCountry: "World"
};
const matchWithNoReportedScore = {
  Id: 1,
  EventID: 1,
  CompletedAt: 1,
  PlayerOneId: 69420,
  PlayerTwoId: 42069,
  PlayerOneScore: 0,
  PlayerTwoScore: 0,
  WinnerId: 69420,
  BracketRound: -6,
  FullRoundText: "Losers' Semi-Finals",
  Tournamentname: "Evolution",
  EventName: "Street Fighter V",
  PlayerOneName: "SixtyNine-420",
  PlayerTwoName: "FourTwenty-69",
  PlayerOnePrefix: "KAPPA",
  PlayerTwoPrefix: "FOG",
  PlayerOneCountry: "Internet",
  PlayerTwoCountry: "World"
};
const searchStatePlayerOrderMatches = {
  playerOne: 69420,
  playerTwo: 42069
};
const searchStatePlayerOrderDiffers = {
  playerOne: 42069,
  playerTwo: 69420
};

beforeEach(() => {
  results = render(HeadToHeadCard);
});

test('if scores are not reported, should render W and L', async () => {
  await results.rerender({ props: { state: searchStatePlayerOrderMatches, match: matchWithNoReportedScore } });
  const card = cardQuery(results);
  expect(card.player.one.name).toHaveTextContent(matchWithNoReportedScore.PlayerOneName);
  expect(card.player.two.name).toHaveTextContent(matchWithNoReportedScore.PlayerTwoName);
  expect(card.player.one.prefix).toHaveTextContent(matchWithNoReportedScore.PlayerOnePrefix);
  expect(card.player.two.prefix).toHaveTextContent(matchWithNoReportedScore.PlayerTwoPrefix);
  expect(card.player.one.score).toHaveTextContent("W");
  expect(card.player.two.score).toHaveTextContent("L");
  expect(card.winner.wedge).toHaveClass('player-one');
});

test('if scores are reported, should render the scores even though javascripts hates 0s', async () => {
  await results.rerender({ props: { state: searchStatePlayerOrderMatches, match: matchWithZeroScore } });
  const card = cardQuery(results);
  expect(card.player.one.name).toHaveTextContent(matchWithZeroScore.PlayerOneName);
  expect(card.player.two.name).toHaveTextContent(matchWithZeroScore.PlayerTwoName);
  expect(card.player.one.prefix).toHaveTextContent(matchWithZeroScore.PlayerOnePrefix);
  expect(card.player.two.prefix).toHaveTextContent(matchWithZeroScore.PlayerTwoPrefix);
  expect(card.player.one.score).toHaveTextContent(matchWithZeroScore.PlayerOneScore);
  expect(card.player.two.score).toHaveTextContent(matchWithZeroScore.PlayerTwoScore);
  expect(card.winner.wedge).toHaveClass('player-one');
});

test('if the "search order" is different than the player order in the DB, mirror everything to keep the "rendered order" consistent', async () => {
  await results.rerender({ props: { state: searchStatePlayerOrderDiffers, match: matchWithZeroScore } });
  const card = cardQuery(results);
  expect(card.player.one.name).toHaveTextContent(matchWithZeroScore.PlayerTwoName);
  expect(card.player.two.name).toHaveTextContent(matchWithZeroScore.PlayerOneName);
  expect(card.player.one.prefix).toHaveTextContent(matchWithZeroScore.PlayerTwoPrefix);
  expect(card.player.two.prefix).toHaveTextContent(matchWithZeroScore.PlayerOnePrefix);
  expect(card.player.one.score).toHaveTextContent(matchWithZeroScore.PlayerTwoScore);
  expect(card.player.two.score).toHaveTextContent(matchWithZeroScore.PlayerOneScore);
  expect(card.winner.wedge).toHaveClass('player-two');
});
