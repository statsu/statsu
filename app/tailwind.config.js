module.exports = {
  theme: {
    fontFamily: {
      display: ['Gilroy', 'sans-serif'],
      body: ['Graphik', 'sans-serif'],
    },
    extend: {
      colors: {
        text: {
          primary: "rgba(255, 255, 255, 0.87)",
          secondary: "rgba(255, 255, 255, 0.6)",
          disabled: "rgba(255, 255, 255, 0.38)",
        },
        actions: {
          active: "rgba(255, 255, 255, 0.87)",
          hover: "rgba(255, 255, 255, 0.08)",
          selected: "rgba(255, 255, 255, 0.16)",
          disabled: "rgba(255, 255, 255, 0.3)",
        },
        bg: "#121212",
        paper: "#222222",
        divider: "rgba(255, 255, 255, 0.12)"
      }
    }
  },
  variants: {},
  plugins: []
}
