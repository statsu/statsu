CREATE EXTENSION pg_trgm;

CREATE TABLE games (
  id SMALLINT PRIMARY KEY,
  name VARCHAR(255)
);

CREATE TABLE events (
  id INTEGER PRIMARY KEY,
  game SMALLINT,
  name VARCHAR(255) NOT NULL,
  tournamentName VARCHAR(255),
  startAt INTEGER,
  endAt INTEGER,
  venueName VARCHAR(255),
  venueAddress VARCHAR(255),
  mapsPlaceId VARCHAR(255),
  smashUrl VARCHAR(255)
);

CREATE TABLE sets (
  id INTEGER PRIMARY KEY,
  eventID INTEGER NOT NULL,
  completedAt INTEGER NOT NULL,
  playerOneId INTEGER NOT NULL,
  playerTwoId INTEGER NOT NULL,
  playerOneScore SMALLINT,
  playerTwoScore SMALLINT,
  playerOneEventPrefix VARCHAR(255),
  playerTwoEventPrefix VARCHAR(255),
  winnerId INTEGER NOT NULL,
  bracketRound SMALLINT,
  fullRoundText VARCHAR(255)
);

CREATE TABLE players (
  id INTEGER PRIMARY KEY,
  prefix VARCHAR (255),
  name VARCHAR (255),
  twitter VARCHAR (255),
  twitch VARCHAR(255),
  country VARCHAR(255)
);

CREATE INDEX idx_set_players ON sets (playeroneid, playertwoid);

CREATE INDEX CONCURRENTLY idx_player_names ON players USING gin (name gin_trgm_ops);
