package main

import (
  "context"
  "github.com/machinebox/graphql"
  "gitlab.com/statsu/statsu/statsu-scripts/graph"
  "log"
  _ "github.com/lib/pq"
  "github.com/jmoiron/sqlx"
  "gitlab.com/statsu/statsu/statsu-api/postgres"
  "time"
  "os"
  "fmt"
)

func main() {
  dbHost := os.Getenv("DB_HOST")
  dbUser := os.Getenv("DB_USER")
  dbName := os.Getenv("DB_NAME")
  dbPass := os.Getenv("DB_PASS")
  authHeader := fmt.Sprintf("Bearer %s", os.Getenv("SMASH_API_KEY"))
  client := graphql.NewClient("https://api.smash.gg/gql/alpha")
  db, err := sqlx.Open("postgres", fmt.Sprintf("host=%s user=%s dbname=%s password=%s sslmode=disable", dbHost, dbUser, dbName, dbPass))
  if err != nil {
      log.Fatalln(err)
  }
  ctx := context.Background()
  // Prepare tournaments by videogame request
  tournamentsByVideogameRequest := graphql.NewRequest(graph.TournamentsByVideogameQuery)
  tournamentsByVideogameRequest.Var("videogameId", 7)
  tournamentsByVideogameRequest.Header.Set("Authorization", authHeader)

  //Prepare tournament request
  tournamentRequest := graphql.NewRequest(graph.TournamentQuery)
  tournamentRequest.Header.Set("Authorization", authHeader)

  // Prepare sets from event request
  setsFromEventRequest := graphql.NewRequest(graph.SetsFromEventQuery)
  setsFromEventRequest.Header.Set("Authorization", authHeader)

  // initialize response types
  var tournamentsByVideogameResponse graph.TournamentsByVideogame
  var tournamentResponse graph.Tournament
  var setsFromEventResponse graph.SetsFromEvent

  for tpage := 1; ; tpage++ {
    tournamentsByVideogameRequest.Var("page", tpage)
    err = client.Run(ctx, tournamentsByVideogameRequest, &tournamentsByVideogameResponse)
    time.Sleep(1 * time.Second)
    for _, tournament := range tournamentsByVideogameResponse.Tournaments.Nodes {
      tournamentRequest.Var("id", tournament.Id)
      err = client.Run(ctx, tournamentRequest, &tournamentResponse);
      time.Sleep(1 * time.Second)
      if err != nil {
        log.Println(err)
      }
      for _, event := range tournamentResponse.Tournament.Events {
        if event.Videogame.Id == 7 {
          tx := db.MustBegin()
          _, err = postgres.InsertEvent(
            tx,
            &postgres.Event{
              event.Id,
              event.Videogame.Id,
              event.Name,
              tournamentResponse.Tournament.Name,
              tournamentResponse.Tournament.StartAt,
              tournamentResponse.Tournament.EndAt,
              tournamentResponse.Tournament.VenueName,
              tournamentResponse.Tournament.VenueAddress,
              tournamentResponse.Tournament.MapsPlaceId,
              tournamentResponse.Tournament.Url,
            })
          tx.Commit()
          if err != nil {
            log.Println(err)
          }

          for page := 1;; page++ {
            time.Sleep(1 * time.Second)
            setsFromEventRequest.Var("page", page)
            setsFromEventRequest.Var("id", event.Id)
            err = client.Run(ctx, setsFromEventRequest, &setsFromEventResponse)
            time.Sleep(1 * time.Second)
            if err != nil {
              log.Fatalln(err)
            }
            if setsFromEventResponse.Event.Sets.Nodes == nil {
              log.Println("finished getting these nodes")
              break
            }
            for _, set := range setsFromEventResponse.Event.Sets.Nodes {
              etx := db.MustBegin()
              winnerID := postgres.EvaluateWinner(
                set.WinnerId,
                set.Slots[0].Entrant.Id,
                set.Slots[1].Entrant.Id,
                set.Slots[0].Entrant.Participants[0].Player.Id,
                set.Slots[1].Entrant.Participants[0].Player.Id,
              )
              _, err = postgres.InsertSet(
                etx,
                &postgres.Set{
                  set.Id,
                  event.Id,
                  set.CompletedAt,
                  set.Slots[0].Entrant.Participants[0].Player.Id,
                  set.Slots[1].Entrant.Participants[0].Player.Id,
                  set.Slots[0].Standing.Stats.Score.Value,
                  set.Slots[1].Standing.Stats.Score.Value,
                  set.Slots[0].Entrant.Participants[0].Prefix,
                  set.Slots[1].Entrant.Participants[0].Prefix,
                  winnerID,
                  set.Round,
                  set.FullRoundText,
                })
              if err != nil {
                log.Fatalln(err)
              }
              postgres.InsertPlayer(
                etx,
                &postgres.Player{
                  set.Slots[0].Entrant.Participants[0].Player.Id,
                  set.Slots[0].Entrant.Participants[0].Player.Prefix,
                  set.Slots[0].Entrant.Participants[0].Player.GamerTag,
                  set.Slots[0].Entrant.Participants[0].User.Authorizations[0].ExternalUsername,
                  set.Slots[0].Entrant.Participants[0].User.Authorizations[1].ExternalUsername,
                  set.Slots[0].Entrant.Participants[0].User.Location.Country,
                },
              )
              postgres.InsertPlayer(
                etx,
                &postgres.Player{
                  set.Slots[1].Entrant.Participants[0].Player.Id,
                  set.Slots[1].Entrant.Participants[0].Player.Prefix,
                  set.Slots[1].Entrant.Participants[0].Player.GamerTag,
                  set.Slots[1].Entrant.Participants[0].User.Authorizations[0].ExternalUsername,
                  set.Slots[1].Entrant.Participants[0].User.Authorizations[1].ExternalUsername,
                  set.Slots[1].Entrant.Participants[0].User.Location.Country,
                },
              )
              etx.Commit()
            }
          }
        }
      }
    }
  }
}
