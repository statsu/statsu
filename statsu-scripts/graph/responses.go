package graph

type SetsFromEvent struct {
  Event struct {
    Name string
    Sets struct {
      Nodes []struct {
        Id int
        FullRoundText string
        Round int
        CompletedAt int
        WinnerId int
        Slots []struct {
          Standing struct {
            Stats struct {
              Score struct {
                Value int
              }
            }
          }
          Entrant struct {
            Id int
            Participants []struct {
              Prefix string
              User struct {
                Location struct {
                  Country string
                }
                Authorizations []struct {
                  // Authorizations[0] = Twitter
                  // Authorizations[1] = Twitch
                  ExternalUsername string
                }
              }
              Player struct {
                Id int
                Prefix string
                GamerTag string
              }
            }
          }
        }
      }
    }
  }
}

type Tournament struct {
  Tournament struct {
    Name string
    StartAt int
    EndAt int
    VenueName string
    VenueAddress string
    MapsPlaceId string
    Url string
    Events []struct {
      Name string
      Id int
      Videogame struct {
        Name string
        Id int
      }
    }
  }
}

type TournamentsByVideogame struct {
  Tournaments struct {
    Nodes []struct {
      Id int
    }
  }
}
