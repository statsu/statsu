package graph

const SetsFromEventQuery string = `
  query SetsFromEvent($id:ID!, $page:Int!) {
    event(id:$id) {
      name
      sets(page: $page perPage: 25) {
        nodes {
          id
          fullRoundText
          round
          completedAt
          winnerId
          slots {
            standing {
              stats {
                score {
                  value
                }
              }
            }
            entrant {
              id
              name
              participants {
                prefix
                user {
                  authorizations(types: [TWITTER, TWITCH]) {
                    externalUsername
                  }
                  location {
                    country
                  }
                }
                player {
                  id
                  prefix
                  gamerTag
                }
              }
            }
          }
        }
      }
    }
  }
`

const TournamentQuery string = `
  query Tournament($id:ID!) {
    tournament(id:$id) {
      name
      startAt
      endAt
      venueName
      venueAddress
      mapsPlaceId
      url(relative:false)
      events {
        id
        name
        tournament {
          name
        }
        videogame {
          name
          id
        }
      }
    }
  }
`

const TournamentsByVideogameQuery string = `
  query TournamentsByVideogame($page: Int!, $videogameId: ID!) {
    tournaments(query: {
      perPage: 50
      page: $page
      sortBy: "startAt desc"
      filter: {
        past: true
        videogameIds: [
          $videogameId
        ]
      }
    }) {
      nodes {
        id
      }
    }
  }
`
