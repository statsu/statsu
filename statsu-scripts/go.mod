module gitlab.com/statsu/statsu/statsu-scripts

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.3.0
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.2.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/statsu/statsu/statsu-api v0.0.0-20200305182409-e38a64db2539
	google.golang.org/appengine v1.6.5 // indirect
)
